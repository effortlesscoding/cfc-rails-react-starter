module Api::V1
  class ItemsController < ApplicationController
    # GET /lists
    def index
      render json: { success: true }
    end
  
    # GET /lists/1
    def show
      render json: { success: false }
    end
  
    # POST /lists
    def create
      render json: { success: true }
    end
  
    # PATCH/PUT /lists/1
    def update
      render json: { success: true }
    end
  
    # DELETE /lists/1
    def destroy
      render json: { success: true }
    end
  end
end